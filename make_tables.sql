create table `Thesis_User`
(
	PK_User_Key int not null auto_increment,
	
	User_ID nvarchar(8) not null,
	PW_Hash nvarchar(65) not null,
	Email nvarchar(45),
	Phone_Number nvarchar(24),
	
	Is_Admin bool default null,
	Is_Coordinator bool default null,
	Is_Rep bool default null,
	Is_Student bool default null,
	Is_Study_Officer bool default null,
	Is_Teacher bool default null,
	
	primary key (PK_User_Key),
	unique (User_ID)
);

create table `Thesis` 
(
	PK_Thesis_Key int not null auto_increment,
	
	Is_Matirity_Ok bool default null,
	Thesis_Name nvarchar(125) not null,
	Thesis_Name_Eng nvarchar(125) not null,
	Thesis_Subject int not null,
	Urkund int,
	Version nvarchar(12) not null default "v0.00",
    Thesis_Time timestamp not null,

	
	FK_Student_Key int not null,
	FK_Peer_Key int not null,
	FK_Rep_Key int not null,
	FK_Sup_1_Key int not null,
	FK_Sup_2_Key int not null,
	FK_Lang_Checker_Key int not null,
	
	foreign key (FK_Student_Key) references Thesis_User(PK_User_Key),
	foreign key (FK_Peer_Key) references Thesis_User(PK_User_Key),
	foreign key (FK_Rep_Key) references Thesis_User(PK_User_Key),
	foreign key (FK_Sup_1_Key) references Thesis_User(PK_User_Key),
	foreign key (FK_Sup_2_Key) references Thesis_User(PK_User_Key),
	foreign key (FK_Lang_Checker_Key) references Thesis_User(PK_User_Key),
	
	primary key (PK_Thesis_Key)
);

create table `Grade`
(
	PK_Grade_Key int not null auto_increment,
	
	`FK_Owner_Key` int not null,
	`FK_Thesis_Key` int not null,
	`Grade_Comment` nvarchar(1025),
	`Form_String` nvarchar(64),
	`Grade_Value` decimal(6,4),
    `Grade_Role` int not null,
    `Grade_Time` timestamp not null,
	
	foreign key (FK_Owner_Key) references Thesis_User(PK_User_Key),
	foreign key (FK_Thesis_Key) references Thesis(PK_Thesis_Key),
    unique(FK_Thesis_Key, Grade_Role, FK_Owner_Key),
	primary key (PK_Grade_Key)
);

create table `Thesis_File`
(
	PK_File_Key int not null auto_increment,
	
	Is_Lang_Ok bool default null,
	File_Location nvarchar(125) not null,
	File_Time timestamp not null,
	
	FK_Thesis_Key int not null,
	
	foreign key (FK_Thesis_Key) references Thesis(PK_Thesis_Key),
	
	primary key (PK_File_Key)
);

create table `Form_Data`
(
	PK_Form_Data_Key int not null auto_increment,
	
	Subject int not null,
	Form_Subset int not null,
	Form_Row int not null,
	Form_Column int not null,
	Cell_Span int not null default 1,
	
	Desc_Fin nvarchar(1025),
	Desc_Eng nvarchar(1025),
	
	unique(Subject, Form_Subset, Form_Row, Form_Column, Cell_Span),
	
	primary key (PK_Form_Data_Key)
);
