<?php

try
{
        $db = new PDO('mysql:host=localhost;dbname=[database-name];charset=utf8','[database-user]', '[database-password]');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
}

catch (PDOException $e)
{
        echo "Error!: " . $e->getMessage() . "<br>";
}