*** Settings ***
Library	Selenium2Library

*** Variables ***
${SERVER}	https://www.lupapiste.fi
${BROWSER}	Firefox
${DELAY}	0
${INVALID USER}	asdf
${INVALID PASSWORD}	asdf

*** Keywords ***
Open Browser To Login Page
	Open Browser	${SERVER}	${BROWSER}
	Maximize Browser Window
	Set Selenium Speed	${DELAY}
	Login Page Should Be Open

Login Page Should Be Open
	Title Should Be	Lupapiste

Go To Login Page
	Go To	${LOGIN URL}
	Lupapiste Should Be Open

Input Username
	[Arguments]	${username}
	Input Text	login-username	${username}

Input Password
	[Arguments]	${password}
	Input Text	login-password	${password}

Submit Credentials
	Click Button	login-button

Click Internal Link With Href
	[Arguments]	${url}
	Element Should Be Visible	xpath=//a[@href='${url}']
	Click element	xpath=//a[@href='${url}']
	Wait Until Keyword Succeeds	20 sec	5 sec	Location Should Be	${server}${url}
