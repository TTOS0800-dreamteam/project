*** Settings ***
Resource	resource.robot

*** Test Cases ***
Empty login test
	[Documentation]	This test checks if clicking the login-button without entering any credentials keeps the user on the login-page. Most "functions" are defined in the "resources.robot" -file. This test should pass.
	Open Browser To Login Page
	Submit Credentials
	Login Page Should Be Open
	[Teardown]	Close Browser

Link click test
	[Documentation]	This test checks if clicking a navigation links can be propely "hooked" onto. This test should pass.
	Open Browser To Login Page
	Click Internal Link With Href	/ohjeet
	[Teardown]	Close Browser

Login testing
	Open Browser    http://www.lupapiste.fi
	Click Element   login-username
	Input Text      login-username  testi@testi.fi
	Click Element   login-password
	Input Text      login-password  testinen
	[Teardown]	Close Browser
	
Test redirection
	Open Browser    http://www.lupapiste.fi
	Click Element   luvanhakija
	[Teardown]	Close Browser

Test login with illegal characters in username and password
	Open Browser    http://www.lupapiste.fi
	Click Element   login-username
	Input Text      login-username  <^testi#@testi.fi>
	Click Element   login-password
	Input Text      login-password  *tes;;___;ttt*
	[Teardown]	Close Browser	
	