*** Settings ***
Library	 Selenium2Library

*** Variables ***
${SERVER}	http://46.101.115.117/project/project/public/index.php
${BROWSER}	Firefox
${DELAY}	0
${USER}		K1234
${PASSWORD}	K1234	
${REGISTER PAGE}	http://46.101.115.117/project/project/public/register.php
${DASHBOARD PAGE}	http://46.101.115.117/project/project/public/dashboard.php
${GRADES PAGE}	http://46.101.115.117/project/project/public/autoform.php?s=1&key=1
${ENG}  http://46.101.115.117/project/project/public/autoform.php?s=1&key=1&lang=eng
${FIN}  http://46.101.115.117/project/project/public/autoform.php?s=1&key=1&lang=fin
${NEW USERNAME}	hartsa
${NEW PASSWORD}	hartsa	
${NEW EMAIL}	k1516@student.jamk.fi
${NEW PHONE}	0400779137
${MARTTI USER}  Martti
${MARTTI PASS}  Martti
${COMMENT}  Ei hyvä
*** Keywords ***
Open Browser To Main Page
	Open Browser	${SERVER}	${BROWSER}
	Set Selenium Speed	${DELAY}
	Main Page Open

Main Page Open
	Location Should Be  ${SERVER}
	
Dashboard Page Open
    Location Should Be  ${DASHBOARD PAGE}
    
Register Page Open
    Location SHould Be  ${REGISTER PAGE}
    
Grades Page Open
    Location Shoud Be   ${GRADES PAGE}
	
Go To Main Page
	Open Browser	${SERVER}
	Main Page Open

Go To Dashboard Page
    Main Page Open
	Click Element	xpath=//input[@name='uid']
	Input Text	xpath=//input[@name='uid']	${NEW USERNAME}
	Click Element	xpath=//input[@name='pwd']
	Input Text	xpath=//input[@name='pwd']	${NEW PASSWORD}
	Click Button	xpath=//input[@value='Log in']
	Wait Until Keyword Succeeds	20 sec	5 sec	Location Should Be	${DASHBOARD PAGE}

Go To Register Page
	Click Element	xpath=//input[@value='Register']
	Wait Until Keyword Succeeds	20 sec	5 sec	Location Should Be	${REGISTER PAGE}

Go To Grades Page
    Dashboard Page Open
    Click Element   xpath=//a[@href='autoform.php?s=1$key=1']
    Wait Until Keyword Succeeds 20 sec  5 sec   Location Should Be  ${GRADES PAGE}

	
Martti Login
    Main Page Open
	Click Element	xpath=//input[@name='uid']
	Input Text	xpath=//input[@name='uid']	${MARTTI USER}
	Click Element	xpath=//input[@name='pwd']
	Input Text	xpath=//input[@name='pwd']	${MARTTI PASS}
	Click Button	xpath=//input[@value='Log in']
	Wait Until Keyword Succeeds	20 sec	5 sec	Location Should Be	${DASHBOARD PAGE}
    
Invalid Log In
	Click Element	xpath=//input[@name='uid']
	Input Text	xpath=//input[@name='uid']	teeee
	Click Element	xpath=//input[@name='pwd']
	Input Text	xpath=//input[@name='pwd']	terve
	Wait Until Keyword Succeeds	20 sec	5 sec	Location Should Be	${SERVER}

Register New User
	Click Element	xpath=//input[@name='username']
	Input Text	xpath=//input[@name='username']	${NEW USERNAME}
	Click Element	xpath=//input[@name='password']
	Input Text	xpath=//input[@name='password']	${NEW PASSWORD}
	Click Element	xpath=//input[@name='re-pwd']
	Input Text	xpath=//input[@name='re-pwd']	${NEW PASSWORD}
	Click Element	xpath=//input[@name='email']
	Input Text	xpath=//input[@name='email']	${NEW EMAIL}
	Click Element	xpath=//input[@name='phone']
	Input Text	xpath=//input[@name='phone']	${NEW PHONE}
	Click Element	xpath=//input[@name='is-admin']
	Click Element	xpath=//input[@name='myButton']
	
Grade Student
    Grades Page Open
    Click Element   3
    Click Element   9
    Click Element   15
    Click Element   21
    Click Element   27
    Click Element   33
    Click Element   39
    Click Element   45
    Click Element   51
    Click Element   57
    Click Element   63
    Click Element   69
    Click Element   75
    Click Element   81
    Click Element   87
    Click Element   93
    Click Element   xpath=//textarea[@name='comment']
    Input Text  xpath=//textarea[@name='comment']   ${COMMENT}
    Click Button   xpath=//input[@name='myButton']
    Wait Until Keyword Succeeds 20 sec  5 sec   Location Should Be  ${DASHBOARD PAGE}
    

Grade Page ButtonEN
    Grades Page Open
    Click Element   xpath=//a[@href='autoform.php?=s1key=1&lang=eng']
    Wait Until Keyword Succeeds 20 sec  5 sec   Location Should Be  ${ENG}    
Grade Page ButtonFIN
    Grades Page Open
    Click Element   xpath=//a[@href='autoform.php?=s1key=1&lang=fin']
    Wait Until Keyword Succeeds 20 sec  5 sec   Location Should Be  ${FIN}
    
Grade Page ButtonBACK
    Click Element   xpath=//a[@href='return_form.php?key=1']
    Wait Until Keyword Succeeds 20 sec  5 sec   Location SHould Be  ${DASHBOARD PAGE}
    
LogOut
	Click Element	xpath=//a[@href='logout.php']
	Wait Until Keyword Succeeds	20 sec	5 sec	Location Should Be	${SERVER}

