<?php
ob_start();
?>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>Loggin in...</title>
</head>
<body>
<?php
// login.php

//include("navbar.php");
session_start();

require_once ('/var/www/db/PasswordLib.phar');
$lib = new PasswordLib\PasswordLib();

if (isset($_POST['uid']) AND isset($_POST['pwd']))
{
	require_once('/var/www/db/db-init.php');
	$uid = $_POST['uid'];
	$pwd = $_POST['pwd'];
	
	$sql = "SELECT * FROM Thesis_User WHERE User_ID = :uid";
	
	$stmt = $db->prepare($sql);
	$stmt->execute(array($uid));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if ($stmt->rowCount() == 1 AND $lib->verifyPasswordHash($pwd, $row['PW_Hash']))
	{
		$_SESSION['login_id'] 	= $_POST['uid'];
		
		$_SESSION['is_admin']			= $row['Is_Admin'];
		$_SESSION['is_coordinator']		= $row['Is_Coordinator'];
		$_SESSION['is_rep']				= $row['Is_Rep'];
		$_SESSION['is_student']			= $row['Is_Student'];
		$_SESSION['is_study_officer']	= $row['Is_Study_Officer'];
		$_SESSION['is_teacher']			= $row['Is_Teacher'];
		
		echo("Session set!<br>");
		#var_dump($_SESSION);
		#echo("Location:http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "dashboard.php");
		header("Location:http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "dashboard.php");
		exit();
	}
	else
	{
		echo "<strong>Authentication error! Redirecting in 3 seconds...</strong><br>\n";
		header("refresh:3; url=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "index.php");
		exit();
	}
}

else
	{
		echo "<strong>No login data! Redirecting in 3 seconds...</strong><br>\n";
		header("refresh:3; url=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "index.php");
		exit();
	}
?>
</body>
</html>
<?php
ob_end_flush();
exit();
