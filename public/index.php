<?php
session_start();
$disabled;
if($_SESSION['login_id']) $disabled = "disabled";
?>
<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>Jamk Thesis Evaluation tool</title>
</head>
<body>
<header>
JAMK Thesis Evaluation tool
</header>
<fieldset class="myLogin">
<legend>Login</legend>
<form method="post" action="login.php">

<?php 
if($_SESSION['login_id']) 
{
	echo "<p><strong>Already logged in!</strong></p>";
	if($lang == 'fin') 
		echo('<a href="dashboard.php">Takaisin kojelaudalle</a>');
	else echo('<a href="dashboard.php">Back to dashboard</a>');
}
?>

<p class="fieldTitle">Username:</p>
<br>
<input class="loginField" type="text" name="uid" autofocus="" maxlength="12" required="" <?php echo $disabled;?>>
<br>
    
<p class="fieldTitle">Password:</p>
<br>
<input class="loginField" type="password" name="pwd" required="" <?php echo $disabled;?>>
<br>
<input class="formButton" type="submit" value="Log in" <?php echo $disabled;?>>

</form>

<form action="register.php">
<input class="formButton" type="submit" value="Register" <?php echo $disabled;?>>
</form>
</fieldset>
<footer>
    © DreamTeam
</footer>
<script type="text/javascript">
    window.doorbellOptions = {
        appKey: 'eBayOvt7F46zwes860erF0wYdrWqSKRaeEh4gGxjV513KPim8WI2gcAGK9GhFSi2'
    };
    (function(w, d, t) {
        var hasLoaded = false;
        function l() { if (hasLoaded) { return; } hasLoaded = true; window.doorbellOptions.windowLoaded = true; var g = d.createElement(t);g.id = 'doorbellScript';g.type = 'text/javascript';g.async = true;g.src = 'https://embed.doorbell.io/button/5169?t='+(new Date().getTime());(d.getElementsByTagName('head')[0]||d.getElementsByTagName('body')[0]).appendChild(g); }
        if (w.attachEvent) { w.attachEvent('onload', l); } else if (w.addEventListener) { w.addEventListener('load', l, false); } else { l(); }
        if (d.readyState == 'complete') { l(); }
    }(window, document, 'script'));
</script>
</body>
</html>

<!--<section>
<section class="content_center"><h2> Log in </h2>
Log in here
</section> 
</section> -->
