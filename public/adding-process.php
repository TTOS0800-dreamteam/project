<!DOCTYPE HTML>
<html>

<head>
<meta charset="UTF-8">
<title>[DEV] Registering...</title>
</head>

<body>
<main>
<?php
// adding-process.php

echo "Dumping post data...<br>";
var_dump($_POST);

require_once('/var/www/db/db-init.php');

$subject 		= isset($_POST['Subject'])		? $_POST['Subject']		: null;
$subset			= isset($_POST['Subset'])		? $_POST['Subset']		: null;
$form_row		= isset($_POST['Form_Row'])		? $_POST['Form_Row']	: null;
$form_column 	= isset($_POST['Form_Column'])	? $_POST['Form_Column']	: null;
$cell_span		= isset($_POST['Cell_Span'])	? $_POST['Cell_Span']	: null;
$desc_fin		= isset($_POST['Desc_Fin'])		? $_POST['Desc_Fin']	: null;
$desc_eng		= isset($_POST['Desc_Eng'])		? $_POST['Desc_Eng']	: null;


$pre_sql = <<<SQLEND
SELECT *
FROM
	Form_Data
WHERE
	Subject LIKE :f1 
	AND
	Form_Subset LIKE :f2
	AND
	Form_Row LIKE :f3
	AND
	Form_Column LIKE :f4
SQLEND;
	
$pre_stmt = $db->prepare($pre_sql) or die("Server-side error 1: could not prepare required queries.");
$pre_stmt->execute(array(
					':f1' 	=> $subject, 
					':f2' 	=> $subset, 
					':f3' 	=> $form_row, 
					':f4' 	=> $form_column)) or die("Server-side error 2: could not execute required queries.");
					
$row = $pre_stmt->fetch(PDO::FETCH_ASSOC);

echo("<p>Row fetched!</p>");

if($row)
{
	echo("<p><strong>Slot alreadu taken!</strong><br>Redirecting in 3 seconds...</p>");
	header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "add.php");
}

else
{
	try 
	{
		$sql = <<<SQLEND
		INSERT INTO 
			Form_Data (Subject, Form_Subset, Form_Row, Form_Column, Cell_Span, Desc_Fin, Desc_Eng)
		VALUES 
			(:f1,:f2,:f3,:f4,:f5,:f6,:f7)
SQLEND;
		$stmt = $db->prepare($sql) or die("Server-side error 1: could not prepare required queries.");
		$stmt->execute(array(
					':f1' 	=> $subject, 
					':f2' 	=> $subset, 
					':f3' 	=> $form_row, 
					':f4' 	=> $form_column,
					':f5'	=> $cell_span,
					':f6'	=> $desc_fin,
					':f7'	=> $desc_eng)) or die("Server-side error 2: could not execute required queries.");
		echo("<strong>Data entry successful!</strong><br>");
		echo("Redirecting to data entry page in 3 seconds...");
		header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "add.php");
	}
	
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
}

?>