<!DOCTYPE HTML>
<html>

<head>
<meta charset="UTF-8">
<title>[DEV] Registering...</title>
</head>

<body>
<main>

<?php

echo "Dumping post data...<br>";
var_dump($_POST);

/*
echo "<br>Require test... ";
require_once('/var/www/html/db/test.php');
*/

echo "<br>Connecting to database... ";

require_once('/var/www/db/db-init.php');
echo "<br><br>";

echo "Finding password library... ";
require_once('/var/www/db/PasswordLib.phar');
echo "Library found!<br><br>";

echo "Creating a library object... ";
$lib = new PasswordLib\PasswordLib();
echo "Library object created!<br><br>";

echo "Starting registration...<br>";
echo "Creating variables... ";

$username 	= isset($_POST['username'])	? $_POST['username']	: null;
$password	= isset($_POST['password'])	? $_POST['password']	: null; 
$re_pwd		= isset($_POST['re-pwd'])	? $_POST['re-pwd']		: null; 
$email		= isset($_POST['email'])	? $_POST['email']		: null;
$phone		= isset($_POST['phone'])	? $_POST['phone']		: null;

$is_admin			= isset($_POST['is-admin'])			? $_POST['is-admin']			: null;
$is_coordinator		= isset($_POST['is-coordinator'])	? $_POST['is-coordinator']		: null;
$is_rep				= isset($_POST['is-rep'])			? $_POST['is-rep']				: null;
$is_student			= isset($_POST['is-student'])		? $_POST['is-student']			: null;
$is_study_officer	= isset($_POST['is-study-officer'])	? $_POST['is-study-officer']	: null;
$is_teacher			= isset($_POST['is-teacher'])		? $_POST['is-teacher']			: null;

echo "Variables created!<br>";

if($username && $password && $re_pwd)
{
	echo "Register data received!<br>";
	
	if ($password)
	{
		if (preg_match("/^[A-Za-z0-9]{4,12}$/", $username))
		{
			echo "Preg_match OK!<br>";
			if ($password)
			{
				echo("Password OK!<br>");
				// Check wether or not a username is already taken
				$uidsql = <<<SQLEND
				SELECT *
				FROM
					Thesis_User
				WHERE
					User_ID LIKE :f1
SQLEND;

				echo("Checking username... ");
				$pre_stmt = $db->prepare($uidsql) or die("Server-side error 1: could not prepare required queries.");
				$pre_stmt->execute(array(':f1' => $username)) or die("Server-side error 2: could not execute required queries.");
				
				$row = $pre_stmt->fetch(PDO::FETCH_ASSOC);
				echo ("Usernames fetched...<br>");
				if ($row)
				{
					echo("<p><strong>Username already taken!</strong><br>Redirecting in 3 seconds...</p>");
					header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "register.php");
				}
				
				else
				{
					echo "Username OK!<br><br>";
					echo "Registering...<br>";
					
					$hash = $lib->createPasswordHash($password, '$2a$', array('cost' => 12))  or die("Server-side error 3: password could not be created.");
					echo "Password hash created!<br>";
					
					try
					{
						$sql = <<<SQLEND
						INSERT INTO 
							Thesis_User (User_ID, PW_Hash, Email, Phone_Number, Is_Admin, Is_Coordinator, Is_Rep, Is_Student, Is_Study_Officer, Is_Teacher)
						VALUES 
							(:f1,:f2,:f3,:f4,:f5,:f6,:f7,:f8,:f9,:f10)
SQLEND;
						echo "SQL query created!<br>";
						$stmt = $db->prepare($sql) or die("Server-side error: could not prepare required queries.");
						echo "Query prepared!<br>";
						
						$stmt->execute(array(
						':f1' 	=> $username, 
						':f2' 	=> $hash, 
						':f3' 	=> $email, 
						':f4' 	=> $phone, 
						':f5' 	=> $is_admin, 
						':f6' 	=> $is_coordinator, 
						':f7' 	=> $is_rep, 
						':f8' 	=> $is_student, 
						':f9' 	=> $is_study_officer, 
						':f10' 	=> $is_teacher)) or die("Server-side error: could not execute required queries.");
						echo("<strong>Registeration successful!</strong><br>");
						echo("Redirecting to home page in 3 seconds...");
						header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "index.php");
					}
					
					catch(PDOException $e)
					{
						echo "Error: " . $e->getMessage();
					}
				}
				
			}
		
			else
			{
				echo("<strong>Password not set!</strong><br>");
				echo("Redirecting in 3 seconds...");
				header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "register.php");
			}
		}
		
		else
		{
			echo("<strong>Username didn't match the rules!</strong><br>");
			echo("[A-Za-z0-9]{4-12}<br>");
			echo("Redirecting in 3 seconds...");
			header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "register.php");
		}
	}
	
	else
	{
		echo("<strong>Passwords didn't match!</strong><br>");
		echo("Redirecting in 3 seconds...");
		header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "register.php");
	}
	
}

else
{
	echo "Missing arguments!";
}

?>

</main>
</body>