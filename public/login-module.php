<?php
// login-module.php
 
$status = "";
if (isset($_SESSION['hj_login_id'])) 
{
    $deny = '<p class="fieldTitle"><strong>You are already logged in!</strong></p>';
    $status = "disabled";
}
?>
 
<fieldset <?php echo $status;?>>
<legend>Login</legend>
<form method='post' action='login.php'>
 
<?php 
if (isset($deny)) echo $deny;
?>
 
<p class="fieldTitle">Username:</p>
<br>
<input class="loginField" type='text' name='uid' autofocus maxlength="12" required>
<br>
     
<p class="fieldTitle">Password:</p>
<br>
<input class="loginField" type='password' name='pwd' required>
<br>
<input class="formButton" type='submit' value='Log in'>
</form>
 
<form action='register.php'>
<input class="formButton" type='submit' value='Register'>
</form>
</fieldset>