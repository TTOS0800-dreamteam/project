<?php
// register.php
?>
<!DOCTYPE HTML>
<html>
<head>
<title>[DEV] Register</title>
</head>

<body>
<main>

<h1>Registration page</h1>

<strong>This page is only part of the development version! It will not be shipped with the final product!</strong>

<form method="post" action="registration-process.php">

<p>Username (*):</p>
<input type='text' name='username' autofocus maxlength="6" required>
<br>

<p>Password (*):</p>
<input type='password' name='password' required>
<br>

<p>Password again (*):</p>
<input type='password' name='re-pwd' required>
<br>

<p>Email:</p>
<input type='email' name='email' maxlength="44">
<br>

<p>Phone number:</p>
<input type='text' name='phone' maxlength="23">
<br>

<p>User types:<p>
<input type='checkbox' name='is-admin' value="1">Is_Admin<br>
<input type='checkbox' name='is-coordinator' value="1">Is_Coordinator<br>
<input type='checkbox' name='is-rep' value="1">Is_Rep<br>
<input type='checkbox' name='is-student' value="1">Is_Student<br>
<input type='checkbox' name='is-study-officer' value="1">Is_Study_Officer<br>
<input type='checkbox' name='is-teacher' value="1">Is_Teacher<br>

<br>

<input class="formButton" type='submit' name='myButton' value='Register'>
</form>
</main>
</body>
