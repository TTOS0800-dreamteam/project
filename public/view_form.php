<?php
// autoform.php
session_start();

require_once('/var/www/db/db-init.php');

$id 	= isset($_GET['id']) 	? $_GET['id'] 	: null;
$id2	= isset($_GET['id2'])	? $_GET['id2']	: null;
$lang 	= isset($_GET['lang']) 	? $_GET['lang'] : null;
$comment;

?>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="utf-8">
<title>Jamk Thesis Evaluation tool</title>
</head>
<body>
<header>
JAMK Thesis Evaluation tool
</header>

<main>

<?php
echo('<p><a href="view_form.php?id=' . $id . '?id2=' . $id2 . '&lang=eng">In English</a> <a href="view_form.php?id=' . $id . '?id2=' . $id2 . '&lang=fin">Suomeksi</a> ');

if($lang == 'eng') 
	echo('<a href="dashboard.php">Back to dashboard</a>');
else 
	echo('<a href="dashboard.php">Takaisin kojelaudalle</a>');

echo('</p>');
?>

<fieldset disabled>
<form>

<table>
<tbody>

<tr align="center">
		<td></td>
		<td>0</td>
		<td>1</td>
		<td>2</td>
		<td>3</td>
		<td>4</td>
		<td>5</td>
	</tr>


<?php
if($id)
{
	try
	{
		// checks if the accounts are part of the same thesis group
		$a_sql = <<<SQLEND
		select 
			Form_String,
			Grade_Comment
		from 
			Grade
		where 
			PK_Grade_Key like :f1
			and
			FK_Thesis_Key like 
			(select PK_Thesis_Key from Thesis where
				(
					FK_Student_Key like (select PK_User_Key from Thesis_User where User_ID like :f2) 
					or FK_Peer_Key like (select PK_User_Key from Thesis_User where User_ID like :f3) 
					or FK_Rep_Key like (select PK_User_Key from Thesis_User where User_ID like :f4) 
					or FK_Sup_1_Key like (select PK_User_Key from Thesis_User where User_ID like :f5) 
					or FK_Sup_2_Key like (select PK_User_Key from Thesis_User where User_ID like :f6) 
					or FK_Lang_Checker_Key like (select PK_User_Key from Thesis_User where User_ID like :f7)
				)
				and
				(
					FK_Student_Key like (select PK_User_Key from Thesis_User where User_ID like :f8) 
					or FK_Peer_Key like (select PK_User_Key from Thesis_User where User_ID like :f9) 
					or FK_Rep_Key like (select PK_User_Key from Thesis_User where User_ID like :f10) 
					or FK_Sup_1_Key like (select PK_User_Key from Thesis_User where User_ID like :f11) 
					or FK_Sup_2_Key like (select PK_User_Key from Thesis_User where User_ID like :f12) 
					or FK_Lang_Checker_Key like (select PK_User_Key from Thesis_User where User_ID like :f13)
				)
			)
SQLEND;

		$a_stmt = $db->prepare($a_sql) or die("Server-side error 1: could not prepare required queries.");
		$a_stmt->execute(array(
							':f1' 	=> $id,
							':f2' 	=> $_SESSION['login_id'],
							':f3' 	=> $_SESSION['login_id'],
							':f4' 	=> $_SESSION['login_id'],
							':f5' 	=> $_SESSION['login_id'],
							':f6' 	=> $_SESSION['login_id'],
							':f7' 	=> $_SESSION['login_id'],
							':f8' 	=> $id2,
							':f9' 	=> $id2,
							':f10' 	=> $id2,
							':f11' 	=> $id2,
							':f12' 	=> $id2,
							':f13'	=> $id2)) or die("Server-side error 2: could not execute required queries.");
							
		$count = $a_stmt->rowCount();
		if($count == 1)	// If a person is in a group and the person has already graded the thesis, there should be EXACTLY one row
		{
			$a_row = $a_stmt->fetch(PDO::FETCH_ASSOC);
			
			$form_arr = str_split($a_row['Form_String']);
			$comment = $a_row['Grade_Comment'];
			
			$sql = <<<SQLEND
			SELECT *
			FROM
				Form_Data
			WHERE
				Subject LIKE :f1 
			ORDER BY
				Form_Subset, Form_Row, Form_Column ASC
SQLEND;
	
			$stmt = $db->prepare($sql) or die("Server-side error 1: could not prepare required queries.");
			$stmt->execute(array(':f1' => $form_arr[0])) or die("Server-side error 2: could not execute required queries.");
			
			$c_row = -1;
			$last_subset = 0;
			$radio_name = 0;
			$radio_id = 1;
			
			while($row = $stmt->fetch(PDO::FETCH_ASSOC))
			{
				if($row['Form_Row'] != $c_row)
				{
					$c_row++;
					echo("<tr>");
					
					if($row['Cell_Span'] != 7) $radio_name++;
				}
				
				echo('<td colspan="' . $row['Cell_Span'] . '">');
				
				if($row['Form_Row'] != 0 && $row['Form_Column'] >= 0)
				{
					$checked;
					if($row['Form_Column'] == $form_arr[$radio_name])
						$checked = "checked";
					else
						$checked = "";
					
					echo('<input type="radio" name="r' . $radio_name . '" value="' . $row['Form_Column'] . '" id="' . $radio_id  . '" ' . $checked . '>');
					
					if($lang == 'eng') 
						echo('<label for="' . $radio_id . '">' . $row['Desc_Eng'] . '</label>');
					else 
						echo('<label for="' . $radio_id . '">' . $row['Desc_Fin'] . '</label>');
					$radio_id++;
				}
				
				else
				{
					if($lang == 'eng') 
						echo($row['Desc_Eng']);
					else 
						echo($row['Desc_Fin']);
				}
				
				echo("</td>");
				
				if($row['Form_Row'] != $c_row)
				{
					echo("</tr>");
				}
				
				if($row['Form_Subset'] != $last_subset)
				{
					$last_subset = $row['Form_Subset'];
					$c_row = 0;
				}
			}
		}
		else
		{
			echo "<strong>Invalid arguments / Autenthication error! Redirecting in 3 seconds...</strong><br>\n";
			header("refresh:3; url=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "dashboard.php");
		}
	}
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
}
?>	

</tbody>
</table>


<?php
?>

<textarea rows="4" cols="50" maxlength="1024" name='comment'><?php echo htmlspecialchars($comment);?></textarea>

</form>
</fieldset>
</main>
</body>
</html>
