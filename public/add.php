<?php
// add.php
?>
<!DOCTYPE HTML>
<html>
<head>
<title>[DEV] Add</title>
</head>

<body>
<main>

<h1>Add to form</h1>

<strong>This page is only part of the development version! It will not be shipped with the final product!</strong>

<form method="post" action="adding-process.php">

<!--
Subject
Form_Subset
Form_Row
Form_Column
Cell_Span
Desc_Fin
Desc_Eng
-->

<p>Subject (*):</p>
<input type='text' name='Subject' required>
<br>

<p>Form_Subset (*):</p>
<input type='text' name='Subset' required>
<br>

<p>Form_Row (*):</p>
<input type='text' name='Form_Row' required>
<br>

<p>Form_Column (*):</p>
<input type='text' name='Form_Column' required>
<br>

<p>Cell_Span (*):</p>
<input type='text' name='Cell_Span' value="1" required>
<br>

<p>Desc_Fin:</p>
<textarea name='Desc_Fin'></textarea>
<br>

<p>Desc_Eng:</p>
<textarea name='Desc_Eng'></textarea>
<br>

<br>

<input class="formButton" type='submit' name='myButton' value='Add'>

</main>
</body>
