<?php
// return_form.php

session_start();
require_once('/var/www/db/db-init.php');

#var_dump($_POST);
$key = isset($_GET['key'])	? $_GET['key']	: null;

// Check if account is a part of the thesis group, fetches Urkund value
try
{
	$a_sql = <<<SQLEND
	select 
		Urkund,
		Thesis_Subject
	from Thesis
	where PK_Thesis_Key like :f1
	and
	(
		FK_Student_Key like (select PK_User_Key from Thesis_User where User_ID like :f2) 
		or FK_Peer_Key like (select PK_User_Key from Thesis_User where User_ID like :f3) 
		or FK_Rep_Key like (select PK_User_Key from Thesis_User where User_ID like :f4) 
		or FK_Sup_1_Key like (select PK_User_Key from Thesis_User where User_ID like :f5) 
		or FK_Sup_2_Key like (select PK_User_Key from Thesis_User where User_ID like :f6) 
		or FK_Lang_Checker_Key like (select PK_User_Key from Thesis_User where User_ID like :f7)
	)
SQLEND;
	$a_stmt = $db->prepare($a_sql) or die("Server-side error 1: could not prepare required queries.");
	$a_stmt->execute(array(
						':f1' => $key,
						':f2' => $_SESSION['login_id'],
						':f3' => $_SESSION['login_id'],
						':f4' => $_SESSION['login_id'],
						':f5' => $_SESSION['login_id'],
						':f6' => $_SESSION['login_id'],
						':f7' => $_SESSION['login_id'])) or die("Server-side error 2: could not execute required queries.");
						
	$count = $a_stmt->rowCount();
	if($count == 1)	// If a person is in a group, there should be EXACTLY one row
	{
		#echo("<p>Group fetched! Rows: $count</p>");
		$row = $a_stmt->fetch(PDO::FETCH_ASSOC);
		if($row['Urkund'])	// If there's an Urkund percentage, review is ok
		{
			$subject = $row['Thesis_Subject'];
			try
			{
				// Check if the thesis has already been reviewed by the person
				$b_sql = <<<SQLEND
				select PK_Grade_Key from Grade
				where FK_Thesis_Key like :f1
				and FK_Owner_Key like (select PK_User_Key from Thesis_User where User_ID like :f2)
SQLEND;
				$b_stmt = $db->prepare($b_sql) or die("Server-side error 1: could not prepare required queries.");
				$b_stmt->execute(array(
							':f1' => $key,
							':f2' => $_SESSION['login_id'])) or die("Server-side error 2: could not execute required queries.");
							
				$count = $b_stmt->rowCount();
				#echo("<p>Grades fetched! Rows: $count</p>");
				if($count == 0)	// If there is no review, there should be no rows
				{
					$formstring;
					$formstring .= $subject;
					
					$avg;
					$sum = 0;
					$count = 0;
					
					foreach($_POST as $arvo)
					{
						if(is_numeric($arvo))
						{
							$formstring .= $arvo;
							$sum += intval($arvo);
							$count++;
						}
					}
					
					//echo htmlspecialchars($formstring);
					
					$avg = $sum / $count;
					
					//echo("Grading input TBA");
					
					echo("Average: " . $avg . "<br>");
					$grade_sql = <<<SQLEND
					insert into Grade (FK_Owner_Key, FK_Thesis_Key, Grade_Comment, Form_String, Grade_Value, Grade_Role)
					values ((select PK_User_Key from Thesis_User where User_ID like :f1), :f2, :f3, :f4, :f5, :f6)
SQLEND;
					$stmt = $db->prepare($grade_sql) or die("Server-side error 1: could not prepare required queries.");
					$stmt->execute(array(
							':f1' => $_SESSION['login_id'],
							':f2' => $key,
							':f3' => $_POST['comment'],
							':f4' => $formstring,
							':f5' => $avg,
							':f6' => 1)) or die("Server-side error 2: could not execute required queries.");
					
					echo("<p><strong>Grade submitted! Redirecting in 3 seconds...</strong></p>");
					header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "dashboard.php");
				}
				else
				{
					echo ("<p><strong>Grade has already been submitted! Redirecting in 3 seconds...</strong></p>");
					header("Refresh: 3; URL=http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "dashboard.php");
				}
				
			}
			catch(PDOException $e)
			{
				echo "Error: " . $e->getMessage();
			}
		}
		else
			echo("<p><strong>No Urkund value! Not ready to be reviewed!</strong></p>");
	}
	else
		echo("<p><strong>You are not a part of this group!</strong></p>");
}
catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
?>