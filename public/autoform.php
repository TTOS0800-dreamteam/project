<?php
// autoform.php
session_start();

require_once('/var/www/db/db-init.php');

$subject = isset($_GET['s']) 	? $_GET['s'] 	: null;
$lang 	 = isset($_GET['lang']) ? $_GET['lang'] : null;
$key	 = isset($_GET['key'])	? $_GET['key']	: null;

?>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="utf-8">
<title>Jamk Thesis Evaluation tool</title>
</head>
<body>
<header>
JAMK Thesis Evaluation tool
</header>

<main style="padding:0.5em">

<?php
echo('<p> <a href="autoform.php?s=' . $subject . '&key=' . $key . '&lang=eng">In English</a>   <a href="autoform.php?s=' . $subject . '&key=' . $key . '&lang=fin">Suomeksi</a>  ');

if($lang == 'eng') 
	echo(' <a href="dashboard.php">Back to dashboard</a> ');
else 
	echo(' <a href="dashboard.php">Takaisin kojelaudalle</a> ');

echo('</p>');
?>

<form action="return_form.php?key=<?php echo $key;?>" method="post">

<table>
<tbody>

<tr align="center">
		<td></td>
		<td>0</td>
		<td>1</td>
		<td>2</td>
		<td>3</td>
		<td>4</td>
		<td>5</td>
	</tr>


<?php
if($subject)
{
	$sql = <<<SQLEND
	SELECT *
	FROM
		Form_Data
	WHERE
		Subject LIKE :f1 
	ORDER BY
		Form_Subset, Form_Row, Form_Column ASC
SQLEND;

	$stmt = $db->prepare($sql) or die("Server-side error 1: could not prepare required queries.");
	$stmt->execute(array(':f1' => $subject)) or die("Server-side error 2: could not execute required queries.");
	
	// current row
	$c_row = -1;
	$last_subset = 0;
	$radio_name = 0;
	$radio_id = 1;
	
	while($row = $stmt->fetch(PDO::FETCH_ASSOC))
	{
		if($row['Form_Row'] != $c_row)
		{
			$c_row++;
			echo("<tr>");
			
			if($row['Cell_Span'] != 7) $radio_name++;
		}
		
		echo('<td colspan="' . $row['Cell_Span'] . '">');
		
		#echo("<strong>Current row: $c_row; Last subset: $last_subset</strong>");
		
		if($row['Form_Row'] != 0 && $row['Form_Column'] >= 0)
		{
			echo('<input type="radio" name="r' . $radio_name . '" value="' . $row['Form_Column'] . '" id="' . $radio_id . '" required>');
			
			if($lang == 'eng') 
				echo('<label for="' . $radio_id . '">' . $row['Desc_Eng'] . '</label>');
			else 
				echo('<label for="' . $radio_id . '">' . $row['Desc_Fin'] . '</label>');
			$radio_id++;
		}
		
		else
		{
			if($lang == 'eng') 
				echo($row['Desc_Eng']);
			else 
				echo($row['Desc_Fin']);
		}
		
		echo("</td>");
		
		if($row['Form_Row'] != $c_row)
		{
			echo("</tr>");
		}
		
		if($row['Form_Subset'] != $last_subset)
		{
			$last_subset = $row['Form_Subset'];
			$c_row = 0;
		}
	}
}
?>	

</tbody>
</table>


<?php
$text_area = "A short, free-form comment";
if($lang == 'eng') 
	$text_area = "A short, free-form comment";
else 
	$text_area = "Lyhyt, vapaamuotoinen kommentti";
?>
<textarea rows="4" cols="50" maxlength="1024" name='comment' required><?php echo $text_area;?></textarea>

<input class="formButton" type='submit' name='myButton' value='Submit!'>

</form>
</main>
</body>
</html>
