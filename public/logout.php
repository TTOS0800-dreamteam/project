<?php
ob_start();
session_start();
if(isset($_SESSION['login_id']))
{
	session_unset();
	session_destroy();
}
header("Location:http://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']) . '/' . "index.php");
ob_end_flush();
exit;