<!doctype html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title>Jamk Thesis Evaluation tool</title>
</head>
<body>
<header>
JAMK Thesis Evaluation tool
</header>
<!--
    <p>Dashboard
    <a href="logout.php">Log out</a>
    </p>
-->
<?php 
session_start();
require_once('/var/www/db/db-init.php');

if($_SESSION['login_id'])
{
	echo "<div class='userinfo'>";
	
	echo "<p>Logged in as: " . $_SESSION['login_id'] . "</p>";
    
	
	$privs;
	
	if($_SESSION['is_admin']) $privs .= "Admin";
	
	if($_SESSION['is_coordinator'])
	{
		if($privs) $privs .= ", ";
		$privs .= "Coordinator";
	}
	
	if($_SESSION['is_rep'])
	{
		if($privs) $privs .= ", ";
		$privs .= "Client representative";
	}
	
	if($_SESSION['is_student'])
	{
		if($privs) $privs .= ", ";
		$privs .= "Student";
	}
	
	if($_SESSION['is_study_officer'])
	{
		if($privs) $privs .= ", ";
		$privs .= "Stufy Officer";
	}
	
	if($_SESSION['is_teacher'])
	{
		if($privs) $privs .= ", ";
		$privs .= "Teacher";
	}

	echo "<p><strong>Roles: </strong>" . htmlspecialchars($privs) . "</p>";	
	echo '<a href="logout.php">Log out</a>';
	
	echo "</div>";
	echo "<h3 style='text-align:center'>Thesises related to you</h3>";
	
	try
	{
		$sql = <<<SQLEND
		select 
			Thesis.PK_Thesis_Key,
			Thesis.Is_Maturity_Ok, 
			Thesis.Thesis_Name, 
			Thesis.Thesis_Name_Eng, 
			Thesis.Thesis_Subject as "subject",
			Thesis.Urkund, 
			Thesis.Version, 
			Thesis.Thesis_Time, 
			Thesis_User.User_ID as "student", 
			u2.USer_ID as "peer", 
			u3.USer_ID as "rep", 
			u4.USer_ID as "s1", 
			u5.USer_ID as "s2", 
			u6.USer_ID as "lang"  
		from 
			Thesis
		inner join Thesis_User on Thesis.FK_Student_Key = Thesis_User.PK_User_Key
		left join Thesis_User u2 on Thesis.FK_Peer_Key = u2.PK_User_Key
		left join Thesis_User u3 on Thesis.FK_Rep_Key = u3.PK_User_Key
		left join Thesis_User u4 on Thesis.FK_Sup_1_Key = u4.PK_User_Key
		left join Thesis_User u5 on Thesis.FK_Sup_2_Key = u5.PK_User_Key
		left join Thesis_User u6 on Thesis.FK_Lang_Checker_Key = u6.PK_User_Key
		where 
			FK_Student_Key like (select PK_User_Key from Thesis_User where User_ID like :f1) 
			or FK_Peer_Key like (select PK_User_Key from Thesis_User where User_ID like :f2) 
			or FK_Rep_Key like (select PK_User_Key from Thesis_User where User_ID like :f3) 
			or FK_Sup_1_Key like (select PK_User_Key from Thesis_User where User_ID like :f4) 
			or FK_Sup_2_Key like (select PK_User_Key from Thesis_User where User_ID like :f5) 
			or FK_Lang_Checker_Key like (select PK_User_Key from Thesis_User where User_ID like :f6)
SQLEND;
		
		$stmt = $db->prepare($sql) or die("Server-side error 1: could not prepare required queries.");
		$stmt->execute(array(
						':f1' => $_SESSION['login_id'],
						':f2' => $_SESSION['login_id'],
						':f3' => $_SESSION['login_id'],
						':f4' => $_SESSION['login_id'],
						':f5' => $_SESSION['login_id'],
						':f6' => $_SESSION['login_id'])) or die("Server-side error 2: could not execute required queries.");
		
		$users = $stmt->fetchAll(PDO::FETCH_ASSOC);
		
		foreach($users as $row)
		{
			echo "<div class='content_box'>";
			
			echo "<p>";
			echo "Thesis's name: " . htmlspecialchars($row['Thesis_Name']) . "<br>";
			echo "Thesis's English name: " . htmlspecialchars($row['Thesis_Name_Eng']) . "<br>";
			// subject comes here; need to add "CASE... WHEN" for subjects to SQL query

			echo "Thesis subject number: " . htmlspecialchars($row['subject']) . "<br>";
			
			echo "Thesis's owner: ";
			hrefEcho($db, $row['PK_Thesis_Key'], $row['student']);
			
			echo "Peer reviewer: ";
			hrefEcho($db, $row['PK_Thesis_Key'], $row['peer']);
			
			echo "Client representative: ";
			hrefEcho($db, $row['PK_Thesis_Key'], $row['rep']);
			
			echo "1st supervisor: ";
			hrefEcho($db, $row['PK_Thesis_Key'], $row['s1']);
			
			echo "2nd supervisor: ";
			hrefEcho($db, $row['PK_Thesis_Key'], $row['s2']);
			
			echo "Language inspector: ";
			hrefEcho($db, $row['PK_Thesis_Key'], $row['lang']);
			
			echo "Maturity check OK: " . htmlspecialchars($row['Is_Matirity_Ok']) . "<br>";
			echo "Urkund percentage: " . htmlspecialchars($row['Urkund']) . "%<br>";
			echo "Thesis version: " . htmlspecialchars($row['Version']) . "<br>";
			echo "Last updated: " . htmlspecialchars($row['Thesis_Time']) . "<br>";
			echo '<a href="autoform.php?s=' . htmlspecialchars($row['subject']) . '&key=' . $row['PK_Thesis_Key'] . '">Link to grading form</a>';

			echo "</p>";
			echo "</div>";
		}
	}
	
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
	
	//echo '<p><a href="autoform.php?s=1" target="_blank">[BETA] Link to grading form</a></p>';
}

else
{
	echo "<p><strong>You are not logged in!</strong></p>";
	echo '<p><a href="index.php">Back to login page</a></p>';
}

#var_dump($_SESSION); 

function hrefEcho($db, $key, $user)
{
	try
	{
		//echo $key . " : " . $user;
		$grade_sql = <<<SQLEND
		select 
			PK_Grade_Key,
			Grade_Value
		from 
			Grade
		where 
			FK_Thesis_Key like :f1
			and FK_Owner_Key like (select PK_User_Key from Thesis_User where User_ID like :f2)
SQLEND;

		$grade_stmt = $db->prepare($grade_sql) or die("Server-side error 1: could not prepare required queries.");
		$grade_stmt->execute(array(
			':f1' => $key,
			':f2' => $user)) or die("Server-side error 2: could not execute required queries.");
		
		if($grade_stmt->rowCount() == 1)
		{
			$row = $grade_stmt->fetch(PDO::FETCH_ASSOC);
			
			$link_string = "<a href='view_form.php?id=" . htmlspecialchars($row['PK_Grade_Key']) . "&id2=" . htmlspecialchars($user) . "'>" . htmlspecialchars($user) . " (avg: " . htmlspecialchars($row['Grade_Value']) . ")</a><br>";
			echo $link_string;
		}
		else
		{
			$link_string = "<a>" . htmlspecialchars($user) . "</a><br>";
			echo $link_string;
		}
	}
	
	catch(PDOException $e)
	{
		echo "Error: " . $e->getMessage();
	}
}
?>



<footer>
© DreamTeam 2017
</footer>
</body>
</html>
