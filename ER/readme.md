The ER-diagrams are made with ER-assistant [http://highered.mheducation.com/sites/0072942207/student_view0/e_r_assistant.html](http://highered.mheducation.com/sites/0072942207/student_view0/e_r_assistant.html). This might be required to be installed in order to open the file. Opening the file is currently the only way to properly see chosen datatypes and relationships.

Please note that some field titles are clipped. This will likely be fixed in the future if the ER-diagrams are re-created.
